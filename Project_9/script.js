'use strict';

fetch('https://reqres.in/api/users?per_page=12')
    .then((response) => {
        return response.json();
    })
    .then((body) => {
        console.log('-----------');
        console.log('Пункт №1: Получить данные всех пользователей')
        console.log('-----------');
        body.data.forEach((item) => {
            console.log(body.data);
        })
        console.log('-----------');
        console.log('Пункт №2: Вывести в консоль фамилии всех пользователей в цикле')
        console.log('-----------');
        body.data.forEach((item) => {
            console.log(item?.last_name);
        });
    });

fetch('https://reqres.in/api/users?per_page=12')
    .then((response) => {
        return response.json();
    })
    .then((body) => {
        console.log('-----------');
        console.log('Пункт №3: Вывести все данные всех пользователей, фамилия которых начинается на F')
        console.log('-----------');
        let userListFiltered = body.data.filter((item) => {
            return item?.last_name[0] === 'F';
        })
        console.log(userListFiltered);
    });

fetch('https://reqres.in/api/users?per_page=12')
    .then((response) => {
        return response.json();
    })
    .then((body) => {
        console.log('-----------');
        console.log('Пункт №4: Вывести следующее предложение: Наша база содержит данные следующих пользователей:  и далее в этой же строке через запятую имена и фамилии всех пользователей. Использовать метод reduce')
        console.log('-----------');
        let userListReduce = body.data.reduce((acc, item) => {
            acc = acc + item.first_name + ' ' + item.last_name + ', ';
            return acc;
        }, '');
        console.log('Наша база содержит данные следующих пользователей:', userListReduce);
    });

fetch('https://reqres.in/api/users?per_page=12')
    .then((response) => {
        return response.json();
    })
    .then((body) => {
    console.log('-----------');
    console.log('Пункт №5: Вывести названия всех ключей в объекте пользователя')
    console.log('-----------');
    Object.entries(body.data[0])?.forEach(([key]) => {
        console.log('Ключ:', key);
    });
})