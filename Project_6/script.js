let num1 = +(prompt('Введите первое число'));
let operation = prompt('Введите знак: +, -, / или *');
let num2 = +(prompt('Введите второе число'));

if (isNaN(num1) || isNaN(num2)) {
    console.log('Некорректный ввод чисел');
} else if (!num1) {
    console.log('Первое число не указано');
} else if (!num2) {
    console.log('Второе число не указано');
} else if (!operation) {
    console.log('Не введён знак');
} else if (operation !== '+' && '-' && '/' && '*') {
    console.log('Программа не поддерживает такую операцию');
} else {
    switch (operation) {
        case '+':
            console.log(num1 + num2);
            break;
        case '-':
            console.log(num1 - num2);
            break;
        case '/':
            console.log(num1 / num2);
            break;
        case '*':
            console.log(num1 * num2);
            break;
    }
}