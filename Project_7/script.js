function makeFibonacciFunction() {
    let num1 = 0;
    let num2 = 1;

    function getNextNumber() {
        let num3 = num1 + num2;
        num1 = num2;
        num2 = num3;
        return num2;
    }

    return getNextNumber;
}

const fibonacci = makeFibonacciFunction();

console.log(fibonacci());
console.log(fibonacci());
console.log(fibonacci());
console.log(fibonacci());
console.log(fibonacci());




